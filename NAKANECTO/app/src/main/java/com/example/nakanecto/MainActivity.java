package com.example.nakanecto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = findViewById(R.id.button);
        city = findViewById(R.id.editText);
        tempMain = findViewById(R.id.tempMain);
        oblate = findViewById(R.id.Kak);
        feels_like = findViewById(R.id.feels_like);
        mmtemp  = findViewById(R.id.minMax);

        pres = findViewById(R.id.pressure);
        humid = findViewById(R.id.humidity);

        windspeed = findViewById(R.id.WindSpeed);
        windvektor = findViewById(R.id.WindVektor);

        lon = findViewById(R.id.lon);

        lat = findViewById(R.id.lat);

        lab4 = findViewById(R.id.textView4);
        lab6 = findViewById(R.id.textView6);
        lab7 = findViewById(R.id.textView7);
        constraintLayout = findViewById(R.id.constraintLayout2);
        gifImageView = findViewById(R.id.gifImageView);

        tempMain.setVisibility(View.INVISIBLE);
        oblate.setVisibility(View.INVISIBLE);
        feels_like.setVisibility(View.INVISIBLE);
        mmtemp.setVisibility(View.INVISIBLE);
        pres.setVisibility(View.INVISIBLE);
        humid.setVisibility(View.INVISIBLE);
        windspeed.setVisibility(View.INVISIBLE);
        windvektor.setVisibility(View.INVISIBLE);
        lab4.setVisibility(View.INVISIBLE);
        lab6.setVisibility(View.INVISIBLE);
        lab7.setVisibility(View.INVISIBLE);
        constraintLayout.setVisibility(View.INVISIBLE);
        gifImageView.setVisibility(View.INVISIBLE);


    }

    Button btn1;
    EditText city;
    TextView tempMain;
    TextView oblate;
    TextView feels_like;
    TextView mmtemp;
    TextView pres;
    TextView humid;
    TextView lab6,lab7,lab4;
    TextView windspeed;
    TextView windvektor;
    EditText lon,lat;
    ConstraintLayout constraintLayout;
    String message = "Поле пустое!";
    GifImageView gifImageView;

    public void visibleElements() {
        tempMain.setVisibility(View.VISIBLE);
        oblate.setVisibility(View.VISIBLE);
        feels_like.setVisibility(View.VISIBLE);
        mmtemp.setVisibility(View.VISIBLE);
        pres.setVisibility(View.VISIBLE);
        humid.setVisibility(View.VISIBLE);
        windspeed.setVisibility(View.VISIBLE);
        windvektor.setVisibility(View.VISIBLE);
        lab4.setVisibility(View.VISIBLE);
        lab6.setVisibility(View.VISIBLE);
        lab7.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.VISIBLE);
        gifImageView.setVisibility(View.VISIBLE);
    }


    public void but1(View view) {
        if(city.getText().equals("")) {
            Toast toast = Toast.makeText(this,message,Toast.LENGTH_LONG);
            toast.show();
        } else {

            String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
            JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject main = response.getJSONObject("main");

                        JSONObject wind = response.getJSONObject("wind");
                        String temp = (main.getInt("temp"))+ "°";
                        String feel_like = "Ощущается как " + (main.getInt("feels_like"))+ "°";
                        JSONArray ja=response.getJSONArray("weather");
                        JSONObject index=ja.getJSONObject(0);
                        String descripion = index.getString("description");
                        String kak = (main.getInt("temp_min")) + "°" + " / " + (main.getInt("temp_max"))+ "°" ;
                        String pressure = "Давление" + "\n" + main.getInt("pressure");
                        String humidility = "Влажность" + "\n" + main.getInt("humidity");


                        String windSpeed = "Скорость ветра: " + wind.getInt("speed") + " м/с";
                        String windV = "Направление ветра: " + wind.getInt("deg") + "°";


                        tempMain.setText(temp);
                        feels_like.setText(feel_like);
                        mmtemp.setText((kak));
                        oblate.setText(descripion);
                        pres.setText(pressure);
                        humid.setText(humidility);

                        windspeed.setText(windSpeed);
                        windvektor.setText(windV);


                    } catch (JSONException e) {setTitle(e.getMessage()); } }}, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) { city.setText(error.getMessage());}});
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jo);
            visibleElements();
        }

    }

    public void but2(View view) {
        try {


            if (lat.getText().equals("") || lon.getText().equals("")) {

            } else {

                String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + Float.valueOf(lon.getText().toString()) + "&lon=" + Float.valueOf(lat.getText().toString()) + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
                JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject main = response.getJSONObject("main");
//                    JSONObject visibility = response.getJSONObject("visibility");
                            JSONObject wind = response.getJSONObject("wind");
                            String temp = (main.getInt("temp")) + "°";
                            String feel_like = "Ощущается как " + (main.getInt("feels_like")) + "°";
                            JSONArray ja = response.getJSONArray("weather");
                            JSONObject index = ja.getJSONObject(0);
                            String descripion = index.getString("description");
                            String kak = (main.getInt("temp_min")) + "°" + " / " + (main.getInt("temp_max")) + "°";
                            String pressure = "Давление" + "\n" + main.getInt("pressure");
                            String humidility = "Влажность" + "\n" + main.getInt("humidity");
//                    String VISIBILITY = "Видимость" + "\n" + visibility.toString();
                            String windSpeed = "Скорость ветра: " + wind.getInt("speed") + " м/с";
                            String windV = "Направление ветра: " + wind.getInt("deg") + "°";


                            tempMain.setText(temp);
                            feels_like.setText(feel_like);
                            mmtemp.setText((kak));
                            oblate.setText(descripion);
                            pres.setText(pressure);
                            humid.setText(humidility);
//                    visib.setText(VISIBILITY);
                            windspeed.setText(windSpeed);
                            windvektor.setText(windV);


                        } catch (JSONException e) {
                            setTitle(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        city.setText(error.getMessage());
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(jo);
                visibleElements();
            }
        }catch (Exception exp) {
            setTitle((exp.getMessage()));
        }

    }
}
